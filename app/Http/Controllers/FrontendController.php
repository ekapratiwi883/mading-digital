<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Slider;
use App\Models\Footer;
use App\Models\Berita;
use App\Models\Agenda;
use App\Models\Tamu;
use Illuminate\Support\Facades\Storage;

class FrontendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slider = Slider::where('status', 1)->orderBy('id','ASC')->get();
        $footer = Footer::all();
        $berita = Berita::all();
        $agenda = Agenda::where('status', 1)->paginate('2');
        return view('frontend.index', compact('slider', 'footer', 'berita', 'agenda'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (empty($request->file('image'))) {
            Tamu::create([
            'nama' => $request->nama,
            'instansi' => $request->instansi,
            'keperluan' => $request->keperluan,
            'telpon' => $request->telpon,
            // 'image' => $request->file('$fileName')->store('tamu'),
        ]);
           return redirect('/')->with('success', 'Selamat Datang di RRI Sumenep');
        } else {
            $image = $request->image;
        $folderPath = "upload/";
        
        $image_parts = explode(";base64,", $image);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        
        $image_base64 = base64_decode($image_parts[1]);
        $fileName = uniqid() . '.jpeg';
        
        $file = $folderPath . $fileName;
        Storage::put($file, $image_base64);

        $insert= new Tamu();
        $insert->nama = $request->nama;
        $insert->instansi = $request->instansi;
        
        $insert->keperluan = $request->keperluan;
        $insert->telpon = $request->telpon;
        $insert->image = $request->$fileName;
        $insert->save();
        return redirect('/')->with('success', 'Selamat Datang di RRI Sumenep');
        }
        
        
        // Tamu::create([
        //     'nama' => $request->nama,
        //     'instansi' => $request->instansi,
        //     'keperluan' => $request->keperluan,
        //     'telpon' => $request->telpon,
        //     // 'image' => $request->file('$fileName')->store('tamu'),
        // ]);
        // return redirect('/')->with('success', 'Selamat Datang di RRI Sumenep');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}