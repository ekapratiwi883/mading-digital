<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Slider;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slider = Slider::all();
        return view('slider.index', compact('slider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $slider = Slider::all();
        return view('slider.create', compact('slider'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Slider::create([
            
        //     'judul_slider' => $request->judul_slider,
        //     'image' => $request->file('image')->store('slider'),
        //     'status' => $request->status,
        // ]);
        // return redirect('slider')->with('success', 'Data Berhasil Disimpan');

        // $request->validate([
        //     'image' => 'required|mimes:mp4, webm, avi'
        // ]);
        $file= $request->file('media');
        $file->move('upload', $file->getClientOriginalName());
        $file_name=$file->getClientOriginalName();

        $insert= new Slider();
        $insert->judul_slider = $request->judul_slider;
        $insert->media = $file_name;
        $insert->tipe_media = $request->tipe_media;
        $insert->status = $request->status;
        $insert->save();
        return redirect('slider')->with('success', 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $slider = Slider::all();
        return view('slider.update', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (empty($request->file('image'))) {
            $slider = Slider::find($id);
            $slider->update([
            'judul_slider' => $request->judul_slider,
            'status' => $request->status,
            ]);
            return redirect('slider')->with('success', 'Data Berhasil DiUpdate');
        } else {
            $slider = Slider::find($id);
            Storage::delete($slider->image);
            $slider->update([
            'judul_slider' => $request->judul_slider,
            'image' => $request->file('image')->store('slider'),
            'status' => $request->status,
            ]);
            return redirect('slider')->with('success', 'Data Berhasil DiUpdate');
        }
        // $slider = Slider::find($id);
        // $slider->update($request->all());
        // return redirect('slider')->with('success', 'Data Berhasil DiUpdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::find($id);
        $slider->delete();
        return redirect('slider')->with('success', 'Data Berhasil Dihapus');
    }
}