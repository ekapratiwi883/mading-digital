<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Galeri;
use Illuminate\Support\Facades\Storage;

class GaleriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galeri = Galeri::all();
        return view('galeri.index', compact('galeri'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('galeri.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Galeri::create([
            'judul' => $request->judul,
            'image' => $request->file('image')->store('galeri'),
            'keterangan' => $request->keterangan,
        ]);
        return redirect('galeri')->with('success', 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $galeri = Galeri::find($id);

        return view('galeri.update', compact('galeri'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (empty($request->file('image'))) {
            $galeri = Galeri::find($id);
            $galeri->update([
            'judul' => $request->judul,
            'keterangan' => $request->keterangan,
            ]);
            return redirect('galeri')->with('success', 'Data Berhasil DiUpdate');
        } else {
            $galeri = Galeri::find($id);
            Storage::delete($galeri->image);
            $galeri->update([
            'judul' => $request->judul,
            'image' => $request->file('image')->store('galeri'),
            'keterangan' => $request->keterangan,
            ]);
            return redirect('galeri')->with('success', 'Data Berhasil DiUpdate');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $galeri = Galeri::find($id);
        $galeri->delete();
        return redirect('galeri')->with('success', 'Data Berhasil Dihapus');
    }
}