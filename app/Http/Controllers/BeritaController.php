<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Berita;
use App\Models\Kategori;
use Illuminate\Support\Facades\Storage;
class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berita = Berita::all();
        $kategori = Kategori::all();
        return view ('berita.index', compact('berita', 'kategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Berita::create([
            'kategori_id' => $request->kategori_id,
            'judul' => $request->judul,
            'image' => $request->file('image')->store('berita'),
            'deskripsi' => $request->deskripsi,
            'status' => $request->status,
        ]);
        return redirect('berita')->with('success', 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $berita = Berita::all();
        return view('berita.update', compact('berita'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (empty($request->file('image'))) {
            $berita = Berita::find($id);
            $berita->update([
                'kategori_id' => $request->kategori_id,
                'judul' => $request->judul,
                'deskripsi' => $request->deskripsi,
                'status' => $request->status,
            ]);
            return redirect('berita')->with('success', 'Data Berhasil DiUpdate');
        } else {
            $berita = Berita::find($id);
            Storage::delete($berita->image);
            $berita->update([
                'kategori_id' => $request->kategori_id,
                'judul' => $request->judul,
                'image' => $request->file('image')->store('berita'),
                'deskripsi' => $request->deskripsi,
                'status' => $request->status,
            ]);
            return redirect('berita')->with('success', 'Data Berhasil DiUpdate');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = Berita::find($id);
        $berita->delete();
        return redirect('berita')->with('success', 'Data Berhasil Dihapus');
    }
}