<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tamu extends Model
{
    protected $table = 'buku_tamu';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nama', 'instansi', 'keperluan', 'telpon', 'image'
    ];
    use HasFactory;
}
