<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    // use HasFactory;
    protected $table ='berita';
    protected $primaryKey ='id';
    protected $fillable= [
        'kategori_id', 'judul', 'image', 'deskripsi', 'status'
    ];
    public function kategori() {
        return $this->belongsTo(Kategori::class);
    }
    
    
}