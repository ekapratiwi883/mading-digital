<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table= 'slider';
    protected $primaryKey= 'id';
    protected $fillable=[
        'judul_slider', 'media', 'tipe_media', 'status'
    ];
    use HasFactory;
}
