<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
// use App\Http\Controllers\KategoriController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\AgendaController;
use App\Http\Controllers\Buku_TamuController;
// use App\Http\Controllers\BeritaController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\GaleriController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\FooterController;
use App\Http\Controllers\TeknikController;
use App\Http\Controllers\Storage;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', [FrontendController::class, 'index']);
Route::post('/', [FrontendController::class, 'store'])->name('frontend.capture');
Route::get('teknik', [TeknikController::class, 'index'])->name('teknik');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {    
    
    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::resource('user', UserController::class);
    // Route::get('kategori', [KategoriController::class, 'index'])->name('kategori');
    // Route::get('kategori/add', [KategoriController::class, 'create'])->name('kategori.create');
    // Route::resource('kategori', KategoriController::class);
    // Route::post('/kategori/{id}', [KategoriController::class, 'update'])->name('kategori.update');
    // route::get('/kategori/{id}/delete', [KategoriController::class, 'destroy']);

    // Route::resource('berita', BeritaController::class);
    // Route::post('/berita/{id}', [BeritaController::class, 'update'])->name('berita.update');
    // route::get('/berita/{id}/delete', [BeritaController::class, 'destroy']);

    Route::resource('agenda', AgendaController::class);
    Route::post('/agenda/{id}', [AgendaController::class, 'update'])->name('agenda.update');
    Route::get('/agenda/{id}/delete', [AgendaController::class, 'destroy']);
    Route::get('/agendacetak', [AgendaController::class, 'cetak'])->name('agenda.cetak');

    Route::resource('buku_tamu', Buku_TamuController::class);
    Route::post('/buku_tamu/{id}', [Buku_TamuController::class, 'update'])->name('buku_tamu.update');
    Route::get('/buku_tamu/{id}/delete', [Buku_TamuController::class, 'destroy']);
    Route::get('/bukucetak', [Buku_TamuController::class, 'cetak'])->name('tamu.cetak');

    Route::resource('slider', SliderController::class);
    Route::post('/slider/{id}', [SliderController::class, 'update'])->name('slider.update');
    Route::get('/slider/{id}/delete', [SliderController::class, 'destroy']);    

    Route::resource('galeri', GaleriController::class);
    Route::post('/galeri/{id}', [GaleriController::class, 'update'])->name('galeri.update');
    route::get('/galeri/{id}/delete', [GaleriController::class, 'destroy']);

    Route::resource('footer', FooterController::class);
    Route::post('/footer/{id}', [FooterController::class, 'update'])->name('footer.update');
    Route::get('/footer/{id}/delete', [FooterController::class, 'destroy']);


    // Route::get('/berita', function () {
    //     return view('berita.index');
    // })->name('berita');
    
});