<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name'=>'Administrator',
            'email'=>'admin@admin.com',
            // 'email_verified_at'=>Carbon::now(),
            // 'status_user'=>0,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
            'password'=>Hash::make('admin'),
        ]);
        $user = User::create([
            'name'=>'Tiara Septianta',
            'email'=>'tiaraseptiantanast@gmail.com',
            // 'email_verified_at'=>Carbon::now(),
            // 'status_user'=>0,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
            'password'=>Hash::make('September@09'),
        ]);
    }
}