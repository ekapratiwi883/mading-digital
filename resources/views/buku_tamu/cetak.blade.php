<!DOCTYPE html>
<html>
<head>
<style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4e73df;
  color: white;
}
</style>
</head>
<body>

<h3>Data Buku Tamu RRI Sumenep</h3>

<table id="customers">
  <tr>
    <th >No.</th>
    <th>Nama Tamu</th>
    <th>Instansi</th>
    <th>Keperluan</th>
    <th>Telpon</th>
    <th>Image</th>
    <th>created_at</th>
  </tr>
  @php $i=1 @endphp
  @foreach ($tamu as $data)
  <tr>
    <td class="text-center" >{{ $i++ }}</td>
    <td>{{ $data->nama }}</td>
    <td>{{ $data->instansi }}</td>
    <td>{{ $data->keperluan }}</td>
    <td>{{ $data->telpon }}</td>
    <td class="text-center">
      <img src="/upload/upload/{{$data->image}}" width="100px">
    </td>
    <td>{{ $data->created_at }}</td>
  </tr>
  @endforeach
  
</table>

</body>
</html>


