@extends('layouts.master')
@section('content')
  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- DataTales Example -->
      <div class="card shadow mb-4">
          <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Tabel Buku Tamu</h6>
              <div class="button text-end" style="float: right;" >
                <a href="{{ route('tamu.cetak') }}" class="btn btn-success" style="margin-right: 5px" ><i class="bi bi-printer" style="margin-right: 5px;"></i>cetak</a>
                <a href="" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style="float: right;">+ Tambah Agenda</a>
              </div>
          </div>
          <div class="card-body">
              <div class="table-responsive">
                  <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0" >
                      <thead class="text-center">
                          <tr>
                            <th style="width: 2%">No.</th>
                            <th style="width: 16%">Nama Tamu</th>
                            <th style="width: 15%">Instansi</th>
                            <th style="width: 17%">Keperluan</th>
                            <th style="width: 13%">Telpon</th>
                            <th style="width: 16%">Image</th>
                            <th>created_at</th>
                            <th style="width: 14%">Action</th>
                          </tr>
                      </thead>
                      <tbody>
                        @php $i=1 @endphp
                        @foreach ($tamu as $data)
                       
                          <tr>
                              <td class="text-center" >{{ $i++ }}</td>
                              <td>{{ $data->nama }}</td>
                              <td>{{ $data->instansi }}</td>
                              <td>{{ $data->keperluan }}</td>
                              <td>{{ $data->telpon }}</td>
                              <td class="text-center">
                                <img src="/upload/upload/{{$data->image}}">
                              </td>  
                              <td>{{ $data->created_at }}</td>
                              <td class="text-center">
                                <!-- <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#updateModal{{$data->id}}"><I class="bi bi-pencil-square"></I></a> -->
                                <a href="" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $data->id }}"><i class="bi bi-trash"></i></a>
                              </td>
                          </tr>
                               
                        @endforeach
                          
                      </tbody>
                  </table>
              </div>
          </div>
      </div>

  </div>


    @foreach ($tamu as $data)
<div class="modal fade" id="deleteModal{{ $data->id }}" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Delete Data</h5>
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <p class="text-center">Are you sure want to delete?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="/buku_tamu/{{$data->id}}/delete" class="btn btn-danger">Delete</a>
      </div>
    </div>
  </div>
</div>
@endforeach

@include('sweetalert::alert')

@endsection

