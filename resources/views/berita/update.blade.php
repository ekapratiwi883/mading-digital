
<!-- Modal -->
@foreach ($berita as $data)
<div class="modal fade" id="updateModal{{ $data->id }}" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="updateModalLabel">Update Agenda</h5>
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{route('berita.update', $data->id)}}" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="group">
            <label for="kategori_id">Kategori</label>
            <select name="kategori_id" class="form-control">
              <option value="{{$data->kategori_id}}">{{$data->kategori->nama_kategori}}</option>
              @foreach($kategori as $item)
              <option value="{{$item->id}}">{{$item->nama_kategori}}</option>
              @endforeach
            </select>
          </div>
          <div class="group">
            <label for="judul">Judul Berita</label>
            <input type="text" name="judul" class="form-control" id="judul" value="{{$data->judul}}">
          </div>
          <div class="group">
            <label for="image">Image</label>
            <input type="file" name="image" class="form-control" id="image">
            <img src="{{ asset('upload/'.$data->image) }}" width="120px">
          </div>
          <div class="group">
            <label for="deskripsi">Deskripsi</label>
            <textarea name="deskripsi" class="form-control">{{$data->deskripsi}}</textarea>
          </div>
          <div class="group">
            <label for="status">Status</label>
            <select class="default-select wide form-control" name="status">
                    <option value="1">Active</option>
                    <option value="0">Draf</option>
            </select>
          </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endforeach