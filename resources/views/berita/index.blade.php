@extends('layouts.master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

    <!-- DataTales Example -->
      <div class="card shadow mb-4">
          <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Tabel Berita</h6>
              <a href="" class="btn btn-primary" data-toggle="modal" data-target="#createModal" style="float: right;">+ Tambah Berita</a>
          </div>
          <div class="card-body">
              <div class="table-responsive">
                  <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0" >
                      <thead class="text-center">
                          <tr>
                              <th style="width: 2%">No.</th>
                              <th>Kategori</th>
                              <th style="width: 15%">Judul Berita</th>
                              <th style="width: 15%">Image</th>
                              <th style="width: 18%">Deskripsi</th>
                              <th style="width: 14%">Status</th>
                              <th style="width: 14%">Action</th>
                      </thead>
                      <tbody>
                        @php $i=1 @endphp
                        @foreach ($berita as $data)
                       
                          <tr>
                              <td class="text-center" >{{ $i++ }}</td>
                              <td>{{ $data->kategori->nama_kategori }}</td>
                              <td>{{ $data->judul }}</td>
                              <td class="text-center">
                                <img src="{{ asset('upload/'.$data->image) }}" width="120px">
                              </td>
                              <td>{{ $data->deskripsi }}</td>
                              <td class="text-center">
                                @if($data->status == 1)
                                  Active
                                @else
                                  Draf
                                @endif
                              </td>
                              <td class="text-center">
                                <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#updateModal{{$data->id}}"><i class="bi bi-pencil-square"></i></a>
                                <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{$data->id}}"><i class="bi bi-trash"></i></a>
                              </td>
                          </tr>
                               
                        @endforeach
                          
                      </tbody>
                  </table>
              </div>
          </div>
      </div>

  </div>

  <div class="modal fade" id="createModal" tabindex="-1" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
      <div class="modal-content " >
        <div class="modal-header">
          <h5 class="modal-title" id="createModalLabel">Tambah Berita</h5>
          <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form action="{{ route('berita.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="group">
              <label for="kategori_id">Kategori</label>
              <select name="kategori_id" id="kategori_id">
                @foreach ($kategori as $data)
                <option value="{{ $data->id }}">{{ $data->nama_kategori }}</option>
                @endforeach
                
              </select>
            </div>
            <div class="group">
              <label for="judul">Judul Berita</label>
              <input type="text" name="judul" class="form-control" id="judul">
            </div>
            <div class="group">
              <label for="image">Image</label>
              <input type="file" name="image" class="form-control" id="image">
            </div>
            <div class="group">
              <label for="deskripsi">Deskripsi</label>
              <textarea name="deskripsi" placeholder="isi" class="form-control"></textarea> 
            </div>
            <div class="group">
              <label for="status">Status</label>
              <select class="default-select wide form-control" name="status">
                      <option value="1">Active</option>
                      <option value="0">Draf</option>
              </select>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </form>
    </div>
  </div>

  @foreach ($berita as $data)
<div class="modal fade" id="deleteModal{{ $data->id }}" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Delete Berita</h5>
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <p class="text-center">Are you sure want to delete?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a  href="/berita/{{$data->id}}/delete " class="btn btn-danger">Delete</a>
      </div>
    </div>
  </div>
</div>
@endforeach


@include('sweetalert::alert')
@include('berita.update')
@endsection