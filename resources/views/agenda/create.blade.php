
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Agenda</h5>
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{ route('agenda.store') }}" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="group">
            <label for="nama_agenda">Nama Agenda</label>
            <input type="text" name="nama_agenda" class="form-control" id="nama_agenda">
          </div>
          <div class="group">
            <label for="jenis">Jenis Agenda</label>
            <select class="default-select wide form-control" name="jenis">
                    <option selected>Select Agenda</option>
                    <option value="Agenda Tahunan">Agenda Tahunan</option>
                    <option value="Agenda Bulanan">Agenda Bulanan</option>
                    <option value="Agenda Mingguan">Agenda Mingguan</option>
            </select>
          </div>
          <div class="group">
            <label for="tanggal">Tanggal</label>
            <input type="date" name="tanggal" class="form-control" id="tanggal">
          </div>
          
          <div class="group">
            <label for="waktu">Waktu</label>
            <input type="time" name="waktu" placeholder="isi" class="form-control"> 
          </div>
          <div class="group">
            <label for="status">Status</label>
            <select class="default-select wide form-control" name="status">
                    <option value="1">Active</option>
                    <option value="0">Draf</option>
            </select>
          </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>