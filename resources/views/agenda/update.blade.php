
<!-- Modal -->
@foreach ($agenda as $data)
<div class="modal fade" id="updateModal{{ $data->id }}" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="updateModalLabel">Update Agenda</h5>
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{route('agenda.update', $data->id)}}" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="group">
            <label for="nama_agenda">Nama Agenda</label>
            <input type="text" name="nama_agenda" class="form-control" id="nama_agenda" value="{{$data->nama_agenda}}">
          </div>
          <div class="group">
            <label for="jenis">Jenis Agenda</label>
            <select class="default-select wide form-control" name="jenis">
                    <option selected>{{$data->jenis}}</option>
                    <option value="Agenda Tahunan">Agenda Tahunan</option>
                    <option value="Agenda Bulanan">Agenda Bulanan</option>
                    <option value="Agenda Mingguan">Agenda Mingguan</option>
            </select>
          </div>
          <div class="group">
            <label for="tanggal">Tanggal</label>
            <input type="date" name="tanggal" class="form-control" id="tanggal" value="{{$data->tanggal}}">
          </div>
          
          <div class="group">
            <label for="waktu">Waktu</label>
            <input type="time" name="waktu" placeholder="isi" class="form-control" value="{{$data->waktu}}">
          </div>
          <div class="group">
            <label for="status">Status</label>
            <select class="default-select wide form-control" name="status">
                    <option value="1" {{$data->status == '1' ? 'selected' : ''}}>Active</option>
                    <option value="0" {{$data->status == '0' ? 'selected' : ''}}>Draf</option>
            </select>
          </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endforeach