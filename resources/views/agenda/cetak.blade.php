<!DOCTYPE html>
<html>
<head>
<style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4e73df;
  color: white;
}
</style>
</head>
<body>

<h3>Data Agenda RRI Sumenep</h3>

<table id="customers">
  <tr>
    <th>No</th>
    <th>Kategori</th>
    <th>Nama Agenda</th>
    <th>Tanggal</th>
    <th>Waktu</th>
  </tr>
  @php $i=1 @endphp
  @foreach ($agenda as $data)
  <tr>
    <td class="text-center" >{{ $i++ }}</td>
    <td>{{ $data->jenis }}</td>
    <td>{{ $data->nama_agenda }}</td>
    <td class="text-center">{{ $data->tanggal }}</td>
    <td>{{ $data->waktu }}</td>
  </tr>
  @endforeach
  
</table>

</body>
</html>


