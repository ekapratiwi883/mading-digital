
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Galeri</h5>
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{ route('galeri.store') }}" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="group">
            <label for="judul">Judul Gambar</label>
            <input type="text" name="judul" class="form-control" id="judul">
          </div>
          <div class="group">
            <label for="image">Gambar</label>
            <input type="file" name="image" class="form-control" id="image">
          </div>
          <div class="group">
            <label for="keterangan">Keterangan Gambar</label>
            <input type="text" name="keterangan" class="form-control" id="keterangan">
          </div>
        
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>