
<!-- Modal -->
@foreach ($slider as $data)
<div class="modal fade" id="updateModal{{ $data->id }}" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="updateModalLabel">Update Slider</h5>
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{route('slider.update', $data->id)}}" method="POST" enctype="multipart/form-data">
          @csrf
              <div class="group">
             <label for="judul_slider">Judul Slider</label>
            <input type="text" name="judul_slider" class="form-control" id="judul_slider" value="{{$data->judul_slider}}">
            </div>
          <div class="group">
            <label for="media">File</label>
            <input type="file" name="media" class="form-control" id="media" value="{{$data->image}}">
            <img src="{{ asset('upload/'.$data->image) }}" width="120px">
          </div>
          <div class="group">
            <label for="tipe_media">Tipe Media</label>
            <select class="default-select wide form-control" name="tipe_media">
                    <option value="1" {{$data->tipe_media == '1' ? 'selected' : ''}}>Active</option>
                    <option value="0" {{$data->tipe_media == '0' ? 'selected' : ''}}>Draf</option>
            </select>
          </div>
          <div class="group">
            <label for="status">Status</label>
            <select class="default-select wide form-control" name="status">
                    <option value="1" {{$data->status == '1' ? 'selected' : ''}}>Active</option>
                    <option value="0" {{$data->status == '0' ? 'selected' : ''}}>Draf</option>
            </select>
          </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endforeach