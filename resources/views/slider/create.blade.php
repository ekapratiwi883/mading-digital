
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Slider</h5>
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{ route('slider.store') }}" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="group">
            <label for="judul_slider">Judul Slider</label>
            <input type="text" name="judul_slider" class="form-control" id="judul_slider">
          </div>
          <div class="group">
            <label for="media">File</label>
            <input type="file" name="media" class="form-control" id="media">
          </div>
          <div class="group">
            <label for="tipe_media">Tipe File</label>
            <select class="default-select wide form-control" name="tipe_media">
                    <option value="1">Image</option>
                    <option value="0">Video</option>
            </select>
          </div>
          <div class="group">
            <label for="status">Status</label>
            <select class="default-select wide form-control" name="status">
                    <option value="1">Active</option>
                    <option value="0">Draf</option>
            </select>
          </div>
          
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>