@extends('layouts.master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

    <!-- DataTales Example -->
      <div class="card shadow mb-4">
          <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Tabel Kategori</h6>
              <a href="" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style="float: right;">+ Tambah Kategori</a>
          </div>
          <div class="card-body">
              <div class="table-responsive">
                  <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0" >
                      <thead class="text-center">
                          <tr>
                            <th style="width: 2%">No.</th>
                            <th style="width:74%">Nama kategori</th>
                            <th style="width: 14%">Action</th>
                          </tr>
                      </thead>
                      <tbody>
                        @php $i=1; @endphp
                        @foreach ($kategoris as $data )
                            
                          <tr>
                              <td class="text-center" >{{ $i++ }}</td>
                              <td>{{ $data->nama_kategori }}</td>
                              <td class="text-center" >
                                <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#updateModal{{$data->id}}"><i class="bi bi-pencil-square"></i></a>
                                <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $data->id }}"><I class="bi bi-trash"></I></a>
                              </td>
                          </tr>
                          
                        @endforeach 
                      </tbody>
                  </table>
              </div>
          </div>
      </div>

  </div>

  <!-- Modal -->
  @foreach ($kategoris as $data )
<div class="modal fade" id="deleteModal{{ $data->id }}" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Delete Kategori</h5>
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <p class="text-center">Are you sure want to delete?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="/kategori/{{$data->id}}/delete" class="btn btn-danger">Delete</a>
      </div>
    </div>
  </div>
</div>
@endforeach

  <!-- /.container-fluid -->
@include('sweetalert::alert')
@include('kategori.create')
@include('kategori.update')

@endsection