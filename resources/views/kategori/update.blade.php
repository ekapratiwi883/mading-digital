
<!-- Modal -->
@foreach ($kategoris as $data )
<div class="modal fade" id="updateModal{{ $data->id }}" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="updateModalLabel">Update Kategori</h5>
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{route('kategori.update', $data->id)}}" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="group">
            <label for="nama_kategori">Nama Kategori</label>
            <input type="text" name="nama_kategori" value="{{$data->nama_kategori}}" class="form-control" id="nama_kategori">
          </div>
        
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endforeach 