<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" integrity="sha512-1sCRPdkRXhBV2PBLUdRb4tMg1w2YPf37qatUFeS7zlBy7jJI8Lf4VHwWfZZfpXtYSLy85pkm9GaYVYMfw5BC1A=="
     crossorigin="anonymous" referrerpolicy="no-referrer" />
     <!-- Responsive -->
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <!-- Css -->
     <link rel="stylesheet" href="style.css">
     <!-- Charts.js -->
     <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
   
    <title>Teknik</title>
  </head>
  <body>
    <!-- Navbar Medsos -->
    <div class="container">
        <div class="row atas pt-2 fixed-top">
            <div class="col">
                <ul class="d-flex">
                    <li><a href=""><i class="fa-brands fa-facebook" ></i></a></li>
                    <li><a href=""><i class="fa-brands fa-instagram"></i></i></a></li>
                    <li><a href=""><i class="fa-brands fa-twitter"></i></a></li>
                    <li><a href=""><i class="fa-brands fa-youtube"></i></i></a></li>
                </ul>
            </div>
        </div>
      </div>
    <!-- Akhir Navbar Medsos  -->

    <!-- Navbar -->
    <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top mt-5 justify-content-start">
      <a class="navbar-brand" href="#"><img src="img/logo_rri.jpg" width="38" class="rounded-circle" style="margin-left:18px"></a>
      <a class="navbar-brand" href="#"><img src="img/logo_uniba.jfif" width="40" class="rounded-circle" style="margin-left:3px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <h3 style="font-size: 30px;">Mading Digital</h3>
            </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item dropdown justify-content-end">
                <li class="nav-item active">
                  <a class="nav-link" href="{{ url('/') }}"> Home <span class="sr-only">(current)</span></a>
                </li>
                
                <!-- <li class="nav-item">
                  <a class="nav-link " href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Galeri
                  </a>
                </li> -->

                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="bi bi-person-fill"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <a class="dropdown-item" href="{{ route('login') }}">Login</a> 
                      <a class="dropdown-item" href="{{ route('register') }}">Register</a> 
                    </div>
                  </li> 
                  </ul>
            </div>  
              </li>
            </ul>
          </form>
        </div>
      </nav>
    </div>
   <!-- Akhir Navbar -->

   <!-- Chart.js -->
   <div style="width: 900px;">
    <canvas id="myChart"></canvas>
  </div>

  <script>
      const labels = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
      ];
      const data = {
        labels: labels,
        datasets: [{
          label: 'Penilaian Kinerja',
          backgroundColor: ['darkgray','salmon','darkgray','salmon','darkgray','salmon'],
          borderColor: 'black',
          data: [15, 10, 25, 45, 20, 30, 50],
          borderWidth:3
        }]
      };


        const config = {
        type: 'bar',
        data: data,
        options: {
        }
      };


        const myChart = new Chart(
        document.getElementById('myChart'),
        config
      );
  </script>
   <!-- Akhir Charts.js -->

<br><br>

       <!-- Footer -->
    <footer>
        <div class="container text-center">
            <div class="row">
               <div class="col-sm-12">
                <p> Copyright &copy 2022  </i> Mading Digital RRI-INF</p>
               </div> 
            </div>
  
        </div>
    </footer>
      <!-- Akhir Footer -->
  
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>
  </html>