<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" integrity="sha512-1sCRPdkRXhBV2PBLUdRb4tMg1w2YPf37qatUFeS7zlBy7jJI8Lf4VHwWfZZfpXtYSLy85pkm9GaYVYMfw5BC1A=="
     crossorigin="anonymous" referrerpolicy="no-referrer" />
     <!-- Responsive -->
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <!-- Css -->
     <link rel="stylesheet" href="style.css">
     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.26/webcam.min.js"></script>
   
    <title>Mading Digital</title>
  </head>
  <body>
    <!-- Navbar Medsos -->
    <div class="container">
        <div class="row atas pt-2 fixed-top">
            <div class="col">
                <ul class="d-flex">
                    <li><a href="https://www.facebook.com/susi.dini.167"><i class="fa-brands fa-facebook" ></i></a></li>
                    <li><a href="https://instagram.com/pro1rrisumenep?igshid=YmMyMTA2M2Y="><i class="fa-brands fa-instagram"></i></i></a></li>
                    <li><a href="https://mobile.twitter.com/Pro1RRISumenep"><i class="fa-brands fa-twitter"></i></a></li>
                    <li><a href="https://m.youtube.com/channel/UCHoGQJcIMfy85Bydf20Wxrw"><i class="fa-brands fa-youtube"></i></i></a></li>
                </ul>
            </div>

            <div class="col">
              <ul class="d-flex justify-content-end" style="margin-right: 5%"> 
                <li><a href="https://m.youtube.com/channel/UCHoGQJcIMfy85Bydf20Wxrw"><i class="fa-brands fa-youtube"></i></i></a></li>
                <li><a href="https://mobile.twitter.com/Pro1RRISumenep"><i class="fa-brands fa-twitter"></i></a></li> 
                <li><a href="https://instagram.com/pro1rrisumenep?igshid=YmMyMTA2M2Y="><i class="fa-brands fa-instagram"></i></i></a></li> 
                <li><a href="https://www.facebook.com/susi.dini.167"><i class="fa-brands fa-facebook" ></i></a></li>
                 
                  
                                </ul>
          </div>
        </div>
      </div>
    <!-- Akhir Navbar Medsos  -->

    <!-- Navbar -->
    <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top mt-5 justify-content-start">
      <a class="navbar-brand" href="#"><img src="img/logo_rri.jpg" width="38" class="rounded-circle" style="margin-left:18px"></a>
      <a class="navbar-brand" href="#"><img src="img/logo_uniba.jfif" width="44" class="rounded-circle" style="margin-left:3px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <h3 style="font-size: 30px;">Mading Digital</h3>
            </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item dropdown justify-content-end">
                <li class="nav-item active">
                  <a class="nav-link" href="{{ route('teknik') }}"> Teknik <span class="sr-only">(current)</span></a>
                </li>
                
                <!-- <li class="nav-item">
                  <a class="nav-link " href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Galeri
                  </a>
                </li> -->

                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="bi bi-person-fill"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <a class="dropdown-item" href="{{ route('login') }}">Login</a> 
                      <a class="dropdown-item" href="{{ route('register') }}">Register</a> 
                    </div>
                  </li> 
                  </ul>
            </div>  
              </li>
            </ul>
          </form>
        </div>
      </nav>
    </div>
      <!-- Akhir Navbar -->

      <!-- Navbar Tanggal --> 
      <!-- <div class="row bawah pixed-top">
        <div class="col">
            <ul class="d-flex justify-content-end pt-5 ">
              <h4 style="font-size:15px;">09 Agustus 2022</h4>
            </ul>
        </div>     
    </div> -->
    <!-- Akhir Tanggal -->

    <!-- =============== STARTT========================= -->
    <!-- Bagian Gambar Bergerak -->
    
    <!-- Akhir -->
    <div class="container-fluid mt-2">
      <div class="row">
          <div class="col-lg-8">
              <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" >
                  <div class="carousel-inner">
                    @foreach($slider as $key => $row)
                      <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                        @if($row->tipe_media == 1)
                        
                        <img class="d-block w-100" src="{{ asset('upload/' . $row->media) }}" style="height:500px; width: 100%">
                      
                        @else
                        
                         <video class="d-block w-100" src="{{ asset('upload/'.$row->media) }}"loop muted autoplay="autoplay" id="video" style="height:500px; width: 100%"></video>
                       
                        @endif
                        <!-- <video loop autoplay="autoplay" muted  style="height:500px; width: 100%">
                        <source src="{{ asset('upload/' . $row->media) }}" type="video/mp4">
                        </video> -->
                      </div>
                      @endforeach
                  </div>
                  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
          </div>
          <!-- Akhir -->
          
          <!-- Awal Agenda -->
          <div class="col-lg-4 mt-4" style="background-color: #fff">
            <!-- <div class="card">
              <div class="card-header "> -->
                <h4 class="text-center"><b>AGENDA</b></h4>
                <hr>
                @foreach ($agenda as $row)
                <div class="card mb-2">
                  <div class="card-header text-center " style="margin: 0px; background-color: rgb(38, 38, 134)">
                      <h5 class="text-center" style="color: #fff">{{ $row->nama_agenda }}</h5>
                  </div>
                  <div class="card-header mb-0" style="font-weight: bold; font-size: 20px;">
                    <p style="padding-right: 10px"><i class="bi bi-calendar2-check-fill" style="margin-right: 15px"></i>{{ \Carbon\Carbon::parse($row->tanggal)->format('l, j F Y') }}</p>
                    <p><i class="bi bi-alarm-fill" style="margin-right: 15px"></i>{{ $row->waktu }} WIB</p>
                  </div>
               </div>
               
               @endforeach
               
               <button type="button" class="btn btn-dark w-100" data-toggle="modal" data-target="#exampleModal">
                Buku Tamu 
              </button>
            <!-- </div>
              
        </div> -->
                
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Buku Tamu RRI Sumenep</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <form method="POST" action="{{ route('frontend.capture') }}">
                          @csrf
                          <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" name="nama" class="form-control" id="nama">
                          </div>

                          <div class="form-group">
                            <label for="instansi">Asal Instansi</label>
                            <input type="text" name="instansi" class="form-control" id="instansi">
                          </div>
                          <div class="form-group">
                            <label for="keperluan">Keperluan </label>
                            <textarea class="form-control" name="keperluan" id="keperluan" rows="2"></textarea>
                          </div>
                          <div class="form-group">
                            <label for="telpon">No.Hp</label>
                            <input type="text" name="telpon" class="form-control" id="telpon">
                          </div>
                          <div class="row">
                            <div class="form-group">
                            
                            <div class="col-sm-6">
                              <label for="telpon">Ambil Gambar</label>
                              <div id="my_camera">
                                
                              </div>
                              <p>
                                <button type="button" class="btn btn-sm btn-info" onclick="take_picture();">Capture</button>
                              </p>
                            </div>

                          </div>
                          <div class="form-group">
                            <label for="telpon">Hasil Capture</label>
                            <div class="col-sm-6" id="result">
                              
                            </div>
                            <input type="hidden" name="image" class="image-tag">
                          </div>
                          </div>
                          
                         
                       
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                      </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          
        <!-- Akhir Agenda -->
@include('sweetalert::alert')
    </div>
</div>
    <br><br><br><br><br>


<script type="text/javascript">
 
  // $("#carouselExampleControls").carousel({internal: false});
  // document.getElementById('video').addEventListener('ended', myHandler, false);
  
$("#carouselExampleControls").carousel('next');
  // function myHandler(e) {
  //   $("#carouselExampleControls").carousel('next');
  // }
</script>
   
    

    
          
    <!-- Galeri -->
   <!--  <section class="galeri" id="galeri ">
      <div class="container">
        <div class="row">
              <div class="col-sm-12 mt-2 text-center col-sm-offset-1">
                <h2>Gallery</h2>
                <hr>
              </div>
      
              <div class="col-sm-3 ">
                <a href="" class="thumbnail">
                  <img src="img/1.jpg" alt="">
                </a>
              </div>
  
              <div class="col-sm-3">
                <a href="" class="thumbnail">
                  <img src="img/1.jpg" alt="">
                </a>
              </div>

              <div class="col-sm-3">
                <a href="" class="thumbnail">
                  <img src="img/1.jpg">
                </a>
              </div>
          
              <div class="col-sm-3 ">
                <a href="" class="thumbnail">
                  <img src="img/1.jpg">
                </a>
              </div>
  
              <div class="col-sm-3 ">
                <a href="" class="thumbnail">
                  <img src="img/1.jpg">
                </a>
              </div>
  
              <div class="col-sm-3 ">
                <a href="" class="thumbnail">
                  <img src="img/1.jpg">
                </a>
              </div>

              <div class="col-sm-3 ">
                <a href="" class="thumbnail">
                  <img src="img/1.jpg">
                </a>
              </div>
              
              <div class="col-sm-3 ">
                <a href="" class="thumbnail">
                  <img src="img/1.jpg">
                </a>
              </div>
              <div class="col-sm-3 ">
                <a href="" class="thumbnail">
                  <img src="img/1.jpg">
                </a>
              </div>
              <div class="col-sm-3 ">
                <a href="" class="thumbnail">
                  <img src="img/1.jpg">
                </a>
              </div>
              <div class="col-sm-3 ">
                <a href="" class="thumbnail">
                  <img src="img/1.jpg">
                </a>
              </div>
              <div class="col-sm-3 ">
                <a href="" class="thumbnail">
                  <img src="img/1.jpg">
                </a>
              </div>
          </div>
          </div>
  </section> -->
  <!-- Akhir galeri-->

    <!-- Footer -->
    <footer>
      <div class="container-fluid text-center">
        <div class="row">
          <div class="col-lg-10 " style="background-color: rgb(38, 38, 134); color:#fff; font-size :35px; margin-button:5%" >
           
           @php
               $data['footer'] =DB::table('footer')
               ->where('status',1)
               ->get();
           @endphp
           @if (count($data['footer']) > 0 )
           
           <style>
             marquee{
               margin-top: 4px;  
             }
             </style> 
             <marquee >{{ $data['footer'][0]->deskripsi_footer }}</marquee>  
           @endif
 
           
           
          </div>
          <div class="col-lg-2 mt-2 text-center" style="background-color: #fff ">
           <?php 
              $mytime = Carbon\Carbon::now();
              echo $mytime->toRfc850string();
            ?>
          </div>
       </div>

      </div>
  </footer>
    <!-- Akhir Footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  
    <script>
      Webcam.set({
        width: 320,
        height: 240,
        image_format: 'jpeg',
        jpeg_quality: 100
      });
      Webcam.attach('#my_camera');

      function take_picture() {
        Webcam.snap(function(data_uri) {
          $(".image-tag").val(data_uri);

          document.getElementById('result').innerHTML = '<img src="' +data_uri+ '">'
        })
      }
    </script>

  </body>
</html>