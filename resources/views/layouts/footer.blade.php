<!-- Footer -->
<footer class="sticky-footer bg-white">
  <div class="container my-auto">
      <div class="copyright text-center my-auto">
          <span>Copyright  &copy; 2022 Digital Mading RRI-UNIBA</span>
      </div>
  </div>
</footer>
<!-- End of Footer -->