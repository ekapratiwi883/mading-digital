<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
      <div class="sidebar-brand-icon ">
          <!-- <i class="fas fa-laugh-wink"></i> -->
          <img src="img/logo_rri.png" style="width: 50px;">
      </div>
      <div class="sidebar-brand-text mx-3"><sup></sup></div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item active">
      <a class="nav-link " href="{{ route('dashboard') }}"> <!--href="{{ url('/home') }}-->
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
  </li>
  <li class="nav-item active">
    <a class="nav-link " href="{{ url('/user') }}">
        <i class="fas fa-fw fa fa-user"></i>
        <span>User</span></a>
</li>


  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
      Interface
  </div>

  <!--Slider-->
  <li class="nav-item">
    <a class="nav-link" href="{{ url('/slider') }}">
        <i class="fas fa-fw fa fa-question-circle"></i>
        <span>Slider</span></a>
  </li>
  <!-- MAster Data -->
  <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa fa-file"></i>
          <span>Master Data</span>
      </a>
      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ url('/buku_tamu') }}">Buku Tamu</a>
              {{-- <a class="collapse-item" href="{{ url('/berita') }}">Berita</a>
              <a class="collapse-item" href="{{ url('/kategori') }}">Kategori</a> --}}
              <a class="collapse-item" href="{{ url('/agenda') }}">Agenda</a>
          </div>
      </div>
  </li>
  
    {{-- Galeri --}}
    <li class="nav-item ">
        <a class="nav-link " href="{{ url('/galeri') }}">
            <i class="fas fa-fw fa fa-camera"></i>
            <span>Galeri</span>
        </a>
    </li>

    {{-- Footer --}}
    <li class="nav-item">
        <a href="{{ url('/footer') }}" class="nav-link">
            <i class="fas fa-wa fa fa-history"></i>
            <Span>Footer</Span>
        </a>
    </li>


  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  

</ul>
<!-- End of Sidebar -->