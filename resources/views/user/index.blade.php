@extends('layouts.master')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

    <!-- DataTales Example -->
      <div class="card shadow mb-4">
          <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Tabel User</h6>
          </div>
          <div class="card-body">
              <div class="table-responsive">
                  <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0" >
                      <thead class="text-center">
                          <tr>
                              <th style="width: 2%">No.</th>
                              <th style="width:25%">Name</th>
                              <th style="width:32%">E-mail</th>
                              <th>Created</th>
                              <th style="width:14%">Action</th>
                          </tr>
                      </thead>
                      <tbody>
                        @php $i=1; @endphp
                        @foreach ($users as $data)   
                                         
                                <tr>
                                    <td class="text-center" >{{ $i++ }}</td>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->email }}</td>
                                    <td class="text-center">{{ $data->created_at->diffForHumans() }}</td>
                                    <td class="text-center">
                                        <a href="" class="btn btn-warning"><i class="bi bi-pencil-square"></i></a>
                                        <a href="" class="btn btn-danger"><i class="bi bi-trash"></i></a>
                                    </td>
                                </tr>
                        @endforeach
                          
                      </tbody>
                  </table>
              </div>
          </div>
      </div>

  </div>
  <!-- /.container-fluid -->
@endsection