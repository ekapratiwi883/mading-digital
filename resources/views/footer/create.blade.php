
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Footer</h5>
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{ route('footer.store') }}" method="POST">
          @csrf
          <div class="group">
            <label for="deskripsi_footer">Deskripsi footer</label>
            <textarea name="deskripsi_footer" id="deskripsi_footer" placeholder="isi" class="form-control"></textarea>
          </div>
          <div class="group">
            <label for="status">Status</label>
            <select name="status" id="status" class="form-control">
              <option value="1">Publish</option>
              <option value="0">Tidak Publish</option>
            </select>

          </div>
        
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>