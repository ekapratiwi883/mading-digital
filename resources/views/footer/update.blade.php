
<!-- Modal -->
@foreach ($footer as $data )
<div class="modal fade" id="updateModal{{ $data->id }}" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="updateModalLabel">Update Footer</h5>
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{route('footer.update', $data->id)}}" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="group">
            <label for="deskripsi_footer">Deskripsi Footer</label>
            <textarea name="deskripsi_footer" id="deskripsi_footer" class="form-control" >{{$data->deskripsi_footer}}</textarea>
            
          </div>
          <div class="group">
            <label for="status">Status</label>
            <select class="form-control" name="status" id="status">
              <option value="1"{{ (1 == $data->status) ? 'selected' : '' }}>Publish</option>
              <option value="0"{{ (0 == $data->status) ? 'selected' : '' }}>Tidak Publish</option>
            </select>
          </div>
        
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endforeach 