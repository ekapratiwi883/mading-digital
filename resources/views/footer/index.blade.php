@extends('layouts.master')
@section('content')
   <!-- Begin Page Content -->
   <div class="container-fluid">

    <!-- DataTales Example -->
      <div class="card shadow mb-4">
          <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Tabel Footer</h6>
              <a href="" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style="float: right;">+ Tambah Kategori</a>
          </div>
          <div class="card-body">
              <div class="table-responsive">
                  <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0" >
                      <thead class="text-center">
                          <tr>
                            <th style="width: 2%">No.</th>
                            <th style="width:59%">Deskripsi Footer</th>
                            <th style="width: 15%">Status</th>
                            <th style="width: 14%">Action</th>
                          </tr>
                      </thead>
                      <tbody>
                        @php $i=1; @endphp
                        @foreach ($footer as $data )
                            
                          <tr>
                              <td class="text-center" >{{ $i++ }}</td>
                              <td>{{ $data->deskripsi_footer }}</td>
                              <td class="text-center">{{ ($data->status == 1) ? "publish" : "tidak publish" }}</td>
                              <td class="text-center" >
                                <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#updateModal{{$data->id}}"><i class="bi bi-pencil-square"></i></a>
                                <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $data->id }}"><I class="bi bi-trash"></I></a>
                              </td>
                          </tr>
                          
                        @endforeach 
                      </tbody>
                  </table>
              </div>
          </div>
      </div>

  </div>

   <!-- Modal -->
   @foreach ($footer as $data )
   <div class="modal fade" id="deleteModal{{ $data->id }}" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
     <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
           <h5 class="modal-title" id="deleteModalLabel">Delete Footer</h5>
           <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
         </div>
         <div class="modal-body">
         <p class="text-center">Are you sure want to delete?</p>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
           <a href="/footer/{{$data->id}}/delete"  type="submit" class="btn btn-danger">Delete</a>
         </div>
       </div>
     </div>
   </div>
   @endforeach

@include('sweetalert::alert')
@include('footer.create')
@include('footer.update')
@endsection